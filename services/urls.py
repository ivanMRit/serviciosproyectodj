from django.urls import path
from . import views
from .views import AgendaView,CustomLoginView, CustomLogoutView,AgregarUsuarioViews, CajaUpdateView,CajaListView,AgregarCajaView,EliminarUsuarioView, TipoServicioListView,editar_cliente, TipoServicioUpdateView, TipoServicioDeleteView, AgregarUsuarioView,ListarUsuariosView, ActualizarUsuarioView


app_name = 'services'

urlpatterns = [
    path('', views.homes, name='home'),
    # Otras rutas de la aplicación
    #clientes vistas
    path('clientes/agregar/',views.agregar_cliente, name='agregar_cli'),
    path('clientes/lista/',views.listar_clientes,name='listacli'),
    path('clientes/editar/<int:cliente_id>/', editar_cliente, name='editar_cliente'),

    #Gerente
    path('gerente/servicios/agregar',views.crear_tipo_servicio, name='agregarserv'),
    path('gerente/servicios', TipoServicioListView.as_view(), name='tipo_servicio_list'),
    path('gerente/servicios/editar/<int:pk>/', TipoServicioUpdateView.as_view(), name='actualizar_servicio'),
    path('gerente/servicios/eliminar/<int:pk>/', TipoServicioDeleteView.as_view(), name='eliminar_servicio'),

    #usuarios
    path('usuarios/agregar', AgregarUsuarioView.as_view(), name='agregar_usuario'),
    path('usuarios/listar', ListarUsuariosView.as_view(), name='listar_usuarios'),
    path('usuarios/editar/<int:pk>/', ActualizarUsuarioView.as_view(), name='actualizar_usuario'),
    path('usuarios/eliminar/<int:pk>/', EliminarUsuarioView.as_view(), name='eliminar_usuario'),

    #caja
    path('caja/agregar', AgregarCajaView.as_view(), name='agregar_caja'),
    path('caja/listar', CajaListView.as_view(), name='listar_cajas'),
    path('caja/editar/<int:pk>/', CajaUpdateView.as_view(), name='editar_caja'),


    #super

    path('agregar_usuariosup/', AgregarUsuarioViews.as_view(), name='agregar_usuariosup'),

    #log

    path('login/', CustomLoginView.as_view(), name='logins'),
    path('logout/', CustomLogoutView.as_view(), name='logouts'),

    #vista agenda

     path('agenda/', AgendaView.as_view(), name='agenda_view'),


    
    
]
