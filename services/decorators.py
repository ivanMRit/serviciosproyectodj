from django.contrib.auth.decorators import user_passes_test

def is_caja_user(user):
    return user.is_authenticated and user.rol == 'caja'

def is_gerente_user(user):
    return user.is_authenticated and user.rol == 'gerente'

caja_login_required = user_passes_test(is_caja_user)
gerente_login_required = user_passes_test(is_gerente_user)


