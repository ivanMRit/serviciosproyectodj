from .models import Cliente,TipoServicio,CustomUser,Caja
from django import forms
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
from django.core.validators import MinLengthValidator

class ClienteForm(forms.ModelForm):
    fecha_atencion = forms.DateField(
        widget=forms.widgets.DateInput(attrs={'type': 'date', 'class': 'form-control'}),
        label='Fecha de Atención'
    )
    hora_atencion = forms.TimeField(
        widget=forms.widgets.TimeInput(attrs={'type': 'time', 'class': 'form-control'}),
        label='Hora de Atención'
    )

    class Meta:
        model = Cliente
        fields = ['nombre', 'fecha_atencion', 'hora_atencion', 'email', 'telefono', 'servicio_realizado', 'personal_atencion']
        labels = {
            'nombre': 'Nombre',
            'fecha_atencion': 'Fecha de Atención',
            'hora_atencion': 'Hora de Atención',
            'email': 'Correo Electrónico',
            'telefono': 'Teléfono',
            'servicio_realizado': 'Servicio Realizado',
            'personal_atencion': 'Personal de Atención',
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'fecha_atencion': forms.widgets.DateInput(attrs={'type': 'date', 'class': 'form-control'}),
            'hora_atencion': forms.widgets.TimeInput(attrs={'type': 'time', 'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'telefono': forms.TextInput(attrs={'class': 'form-control', 'pattern': '\d{10}', 'title': 'El número de teléfono debe contener 10 dígitos'}),
            'servicio_realizado': forms.Select(attrs={'class': 'form-control'}),
            'personal_atencion': forms.Select(attrs={'class': 'form-control'}),
        }


class TipoServicioForm(forms.ModelForm):
    class Meta:
        model = TipoServicio
        fields = ['nombre_servicio', 'quien_ofrece', 'detalles_servicio', 'imagen', 'duration', 'costo_servicio']
        labels = {
            'nombre_servicio': 'Nombre del Servicio',
            'quien_ofrece': 'Quién Ofrece el Servicio',
            'detalles_servicio': 'Detalles del Servicio',
            'imagen': 'Imagen del Servicio',
            'duration': 'Duración del Servicio',
            'costo_servicio': 'Costo del Servicio',
        }

    nombre_servicio = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )

    quien_ofrece = forms.ModelChoiceField(
        queryset=CustomUser.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    detalles_servicio = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 4})
    )

    imagen = forms.ImageField(
        widget=forms.ClearableFileInput(attrs={'class': 'form-control'})
    )

    duration = forms.DurationField(
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )

    costo_servicio = forms.DecimalField(
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )

#  formset para TipoServicio



class CustomUserCreationForm(UserCreationForm):
    ROLES = (
        ('caja', 'Caja'),
        ('gerente', 'Gerente'),
        ('personal_servicio', 'Personal de Servicio'),
    )

    email = forms.EmailField(required=True)
    imagen = forms.ImageField(required=False)
    rol = forms.ChoiceField(choices=ROLES, required=True)

    class Meta:
        model = CustomUser
        fields = ('username', 'email', 'imagen', 'rol', 'password1', 'password2')

    def save(self, commit=True):
        user = super().save(commit=False)
        user.email = self.cleaned_data['email']
        user.imagen = self.cleaned_data['imagen']
        user.rol = self.cleaned_data['rol'].lower() 
        if commit:
            user.save()
        return user


# carrito

class CajaForm(forms.ModelForm):
    class Meta:
        model = Caja
        fields = ['personal_que_atendio', 'servicio_realizado', 'monto_total']

    def clean(self):
        cleaned_data = super().clean()
        personal_que_atendio = cleaned_data.get('personal_que_atendio')
        servicio_realizado = cleaned_data.get('servicio_realizado')

        if personal_que_atendio and servicio_realizado:
            if servicio_realizado.quien_ofrece != personal_que_atendio:
                raise forms.ValidationError("El personal que atendió no ofrece ese servicio.")

        
        costo_servicio = servicio_realizado.costo_servicio if servicio_realizado else 0

        
        cleaned_data['monto_total'] = costo_servicio

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['personal_que_atendio'].queryset = CustomUser.objects.filter(rol='personal_servicio')
       

    #login

class CustomAuthenticationForm(AuthenticationForm):
    class Meta:
        model = CustomUser
        fields = ['username', 'password']

        