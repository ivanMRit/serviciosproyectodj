from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser, Group, Permission, BaseUserManager
from django.utils  import timezone
from django.core.validators import RegexValidator

#core

class CustomUserManager(BaseUserManager):
    def create_user(self, email, username, rol, password=None, **extra_fields):
        if not email:
            raise ValueError('El campo de correo electrónico debe estar configurado.')
        email = self.normalize_email(email)
        user = self.model(email=email, username=username, rol=rol, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, rol, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        return self.create_user(email, username, rol, password, **extra_fields)

#crear 
class CustomUser(AbstractUser):
    ROLES = (
        ('caja', 'Caja'),
        ('gerente', 'Gerente'),
        ('personal_servicio', 'Personal de Servicio'),
    )
    email = models.EmailField(unique=True)
    imagen = models.ImageField(upload_to='media/imagenes_usuarios/', blank=True, null=True)
    rol = models.CharField(max_length=20, choices=ROLES)
    groups = models.ManyToManyField(Group, related_name='customuser_set', blank=True)
    user_permissions = models.ManyToManyField(Permission, related_name='customuser_set', blank=True)

    objects = CustomUserManager()

    def __str__(self):
        return self.username

#crear listo
class TipoServicio(models.Model):
    nombre_servicio = models.CharField(max_length=255)
    quien_ofrece = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    detalles_servicio = models.TextField()
    imagen = models.ImageField(upload_to='imagenes_servicio/', blank=True, null=True)
    duration = models.DurationField(default=timezone.timedelta()) #default 0
    costo_servicio = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return self.nombre_servicio

#crear listo 
class Cliente(models.Model):
    nombre = models.CharField(max_length=100)
    fecha_atencion = models.DateField()
    hora_atencion = models.TimeField()
    email = models.EmailField(unique=False)  
    telefono = models.CharField(max_length=10, validators=[
        RegexValidator(r'^\d{10}$', message='El número de teléfono debe contener 10 dígitos')
    ])
    servicio_realizado = models.ForeignKey(TipoServicio, on_delete=models.SET_NULL, null=True, blank=True)
    personal_atencion = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre

class Caja(models.Model):
    personal_que_atendio = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    servicio_realizado = models.ForeignKey(TipoServicio, on_delete=models.CASCADE, null=True, blank=True)
    monto_total = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    def __str__(self):
        return f"Caja #{self.id} - Atendido por: {self.personal_que_atendio.username}"


#permisos

class CustomPermission(models.Model):
    codename = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.codename