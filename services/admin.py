from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Caja, Cliente, CustomUser, TipoServicio

# Register your models here.

class CustomUserAdmin(UserAdmin):
    model = CustomUser
    list_display = ['username', 'email', 'rol', 'is_staff', 'is_superuser']


admin.site.register(Caja)
admin.site.register(Cliente)
admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(TipoServicio)

