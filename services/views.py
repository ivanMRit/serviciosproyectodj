from django.shortcuts import render,redirect, get_object_or_404
from .forms import ClienteForm,TipoServicioForm, CustomUserCreationForm, CajaForm,CustomAuthenticationForm
from django.views import View
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic.edit import DeleteView, UpdateView
from .models import TipoServicio,Cliente,CustomUser,Caja
from django.contrib.auth.views import LoginView, LogoutView
from .decorators import caja_login_required,gerente_login_required
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse,HttpResponseNotFound, HttpResponseServerError


import logging

logger = logging.getLogger(__name__)



# Create your views here.

def homes(request):
    return render(request, 'serviceshtml/home.html', {})


#vistas cliente
    #agregar
def agregar_cliente(request):
    if request.method == 'POST':
        form = ClienteForm(request.POST)
        if form.is_valid():
            form.save()
            logger.info('Cliente agregado con éxito')
            return redirect('services:home')  
        else:
            logger.warning('Intento de agregar un cliente con datos inválidos')
    else:
        form = ClienteForm()
    return render(request, 'serviceshtml/agregar_cita.html', {'form': form})

    #enlistar
#@gerente_login_required
def listar_clientes(request):
    clientes = Cliente.objects.all()
    return render(request, 'serviceshtml/listar_clientes.html', {'clientes': clientes})

    #actualizr informacion

#@gerente_login_required
def editar_cliente(request, cliente_id):
    cliente = get_object_or_404(Cliente, id=cliente_id)
    if request.method == 'POST':
        form = ClienteForm(request.POST, instance=cliente)
        if form.is_valid():
            form.save()
            logger.info(f'Cliente {cliente_id} editado con éxito por el gerente')
            return redirect('services:listacli')
        else:
            logger.warning(f'Intento de editar el cliente {cliente_id} con datos inválidos por el gerente')
    else:
        form = ClienteForm(instance=cliente)
    return render(request, 'serviceshtml/editar_cliente.html', {'form': form, 'cliente': cliente})



#servicios
    #crear nuevo servicio
#@gerente_login_required
def crear_tipo_servicio(request):
    if request.method == 'POST':
        form = TipoServicioForm(request.POST, request.FILES)
        if form.is_valid():
            # Validar 
            nuevo_tipo_servicio = form.save(commit=False)
            nuevo_tipo_servicio.save()
            logger.info('Nuevo tipo de servicio creado con éxito por el gerente')
            return redirect('services:home') 
        else:
            logger.warning('Intento de crear un nuevo tipo de servicio con datos inválidos por el gerente')
    else:
        form = TipoServicioForm()

    return render(request, 'serviceshtml/agregar_servicio.html', {'form': form})
    #editar servicios

class TipoServicioUpdateView(View):
    template_name = 'serviceshtml/actualizar_servicio.html'

 #   @method_decorator(gerente_login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, pk):
        servicio = get_object_or_404(TipoServicio, pk=pk)
        form = TipoServicioForm(instance=servicio)
        return render(request, self.template_name, {'form': form, 'servicio': servicio})

    def post(self, request, pk):
        servicio = get_object_or_404(TipoServicio, pk=pk)
        form = TipoServicioForm(request.POST, instance=servicio)
        if form.is_valid():
            form.save()
            return redirect('services:tipo_servicio_list') 
        return render(request, self.template_name, {'form': form, 'servicio': servicio})

    #eliminar 

class TipoServicioDeleteView(DeleteView):
    model = TipoServicio
    template_name = 'serviceshtml/eliminar_servicio.html'  
    success_url = reverse_lazy('services:tipo_servicio_list')  

  #  @method_decorator(gerente_login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return TipoServicio.objects.get(pk=self.kwargs['pk'])
    

    
#enlistar 


class TipoServicioListView(ListView):
    model = TipoServicio
    template_name = 'serviceshtml/tipo_servicio_list.html'  
    context_object_name = 'tipos_servicio'
   # @method_decorator(gerente_login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


#usuario (aun en desarrollo / hay problemas)

    #registrar 

class AgregarUsuarioView(View):
    template_name = 'serviceshtml/agregar_user.html'

    #@method_decorator(gerente_login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request):
        form = CustomUserCreationForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            logger.info('Nuevo usuario agregado con éxito por el gerente')

            return redirect('services:tipo_servicio_list')  
        else:
            logger.warning('Intento de agregar un nuevo usuario con datos inválidos por el gerente')

        return render(request, self.template_name, {'form': form})


    #enlistar

class ListarUsuariosView(View):
    template_name = 'serviceshtml/listar_usuarios.html' 

    #@method_decorator(gerente_login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request):
        usuarios = CustomUser.objects.all()
        return render(request, self.template_name, {'usuarios': usuarios})
    
    #actualizar

class ActualizarUsuarioView(View):
    template_name = 'serviceshtml/actualizar_usuario.html' 

    #@method_decorator(gerente_login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs) 

    def get(self, request, pk):
        usuario = get_object_or_404(CustomUser, pk=pk)
        form = CustomUserCreationForm(instance=usuario)
        return render(request, self.template_name, {'form': form, 'usuario': usuario})

    def post(self, request, pk):
        usuario = get_object_or_404(CustomUser, pk=pk)
        form = CustomUserCreationForm(request.POST, instance=usuario)
        if form.is_valid():
            form.save()
            logger.info(f'Usuario {pk} actualizado con éxito por el gerente')

            return redirect('services:listar_usuarios')  
        else:
            logger.warning(f'Intento de actualizar el usuario {pk} con datos inválidos por el gerente')

        return render(request, self.template_name, {'form': form, 'usuario': usuario})
    
    #eliminar

class EliminarUsuarioView(View):
    template_name = 'serviceshtml/eliminar_usuario.html' 

    #@method_decorator(gerente_login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, pk):
        usuario = get_object_or_404(CustomUser, pk=pk)
        return render(request, self.template_name, {'usuario': usuario})

    def post(self, request, pk):
        usuario = get_object_or_404(CustomUser, pk=pk)
        logger.info(f'Usuario {pk} eliminado con éxito por el gerente')
        usuario.delete()
        return redirect('services:listar_usuarios') 


#vistas de caja

    #agregar

class AgregarCajaView(View):
    template_name = 'serviceshtml/agregar_caja.html' 

    #@method_decorator(caja_login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
    

    def get(self, request):
        form = CajaForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = CajaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('services:home')  
        return render(request, self.template_name, {'form': form})

    #enlistar caja

class CajaListView(ListView):
    model = Caja
    template_name = 'serviceshtml/listar_cajas.html'  
    context_object_name = 'cajas'

    #@method_decorator(caja_login_required)
    def dispatch(self, *args, **kwargs):
        try:
            logging.info('Este es un mensaje informativo')
        except Exception as e:
           
            logging.error(f"Error en la vista: {e}")
            return HttpResponse("Error interno del servidor", status=500)

        return super().dispatch(*args, **kwargs)


    #update


class CajaUpdateView(UpdateView):
    model = Caja
    form_class = CajaForm
    template_name = 'serviceshtml/editar_caja.html'  
    success_url = reverse_lazy('services:listar_cajas')

    #@method_decorator(caja_login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        logger.info('Intento de actualización de caja a través de POST')
        return super().post(request, *args, **kwargs)


    #core seguridad
class AgregarUsuarioViews(View):
    template_name = 'serviceshtml/template_super.html'

    #@method_decorator(gerente_login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request):
        form = CustomUserCreationForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = CustomUserCreationForm(request.POST)
        try:
            if form.is_valid():
                user = form.save(commit=False)
                user.is_staff = True
                user.is_superuser = True
                user.save()
                logging.info(f"Nuevo usuario agregado: {user.username} - Rol: {user.rol}")

                return redirect('services:home')

        except Exception as e:
            logging.error(f"Error al agregar usuario: {e}")

        return render(request, self.template_name, {'form': form})
    

    #log

class CustomLoginView(LoginView):
    form_class = CustomAuthenticationForm
    template_name = 'serviceslog/login.html'  
    #success_url = reverse_lazy('services:home')  

class CustomLogoutView(LogoutView):
    template_name = 'serviceslog/logout.html' 
    next_page = reverse_lazy('services:home')


    #vista agenda

class AgendaView(View):
    template_name = 'serviceshtml/agenda.html'  
    def get(self, request, *args, **kwargs):
       
        current_user = request.user

        # Verificar si el usuario tiene el rol adecuado
        if current_user.rol != 'personal_servicio':
        #    # Puedes manejar de manera adecuada cómo responder si el usuario no tiene el rol adecuado
            return render(request, 'errorhtml/error_page.html', {'error_message': 'No tienes permiso para ver esta página'})

       
        agenda = Cliente.objects.filter(personal_atencion=current_user, fecha_atencion=timezone.now().date())

        
        return render(request, self.template_name, {'agenda': agenda})
    

    #manejo de errores
